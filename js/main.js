$('#container-kinhte .owl-carousel').owlCarousel({
	loop: true,
	nav: true,
	dots: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	navText: [
		`<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M7.41 1.41L6 0L0 6L6 12L7.41 10.59L2.83 6L7.41 1.41Z" fill="black"/>
		</svg>`,
		`<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M1.99997 0L0.589966 1.41L5.16997 6L0.589966 10.59L1.99997 12L7.99997 6L1.99997 0Z" fill="black"/>
		</svg>
		`
	],
	responsive: {
		0: {
			items: 1,
		},
		480: {
			items: 2,
		},
		768: {
			items: 3,
		},
		
		1024: {
			items: 4,
		},
		
	}
});
$('.section-slider .owl-carousel').owlCarousel({
	loop: true,
	nav: false,
	dots: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
			margin: 15,
		}
	}
});
$('.slider-video.owl-carousel').owlCarousel({
	loop: false,
	nav: true,
	dots: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	navText:[
		`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="14" height="12"><path d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l160 160c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.2 288 416 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-306.7 0L214.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-160 160z" fill="#121212"/></svg> Quay lại`,
		`Tiếp theo <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="14" height="12"><path d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z" fill="#121212"/></svg>`],
	responsive: {
		0: {
			items: 1,
			margin: 0,
		}
	}
});

$('.slider-video .owl-nav button').click(function() {
	document.querySelectorAll('.video-item').forEach((video) => {
		$('.action-video').removeClass('close')
		video.pause()
	})
})
document.querySelectorAll('.action-video').forEach((button, index) => {
    button.addEventListener('click', () => {
		console.log(index);
		$('.action-video').addClass('close')
        const videos = document.querySelectorAll('.video-item');
        const currentVideo = videos[index];
        const nextVideo = videos[(index + 1) % videos.length];

        currentVideo.play();
        nextVideo.pause();
    });
});

